

public class Persons implements Measurable{
	private String name;
	private int height;
	
	public Persons(String name,int height){
		this.name = name;
		this.height = height;
	}
	public String getName() {
		return name;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.height;
	}

}