package Cal_Tax;
public class Person implements Taxable,Measurable{
	private String name;
	private int salary;
	
	
	public Person(String name,int salary){
		this.name = name;
		this.salary = salary;
	}
	@Override
	public double getMeasure() {
		return this.salary;
	}
	public double getTax() {
		double sum = 0;
		double difference = 0;
		if (this.salary > 0 && this.salary <= 300000) { 
			return sum = this.salary*0.05; 
		} 
		else if (this.salary >= 300001){
			difference = this.salary-300000;
			sum= (300000*0.05)+difference*0.1;
			return sum;
		}
		return sum;
	}
}