package Cal_Tax;

import java.util.ArrayList;

public class TaxCalculator {
	
	public static double average(Measurable objects[]){
		double sum = 0;
		for(Measurable m:objects){
			sum += m.getMeasure();
		}
		if(objects.length>0){
			return sum/objects.length;
		}
		return 0 ;
	}
	public static double sum(ArrayList<Taxable> taxList){
		double sumTax = 0;
		for(Taxable l : taxList){
			sumTax += l.getTax();
		}
		
		return sumTax ;
	}
}
