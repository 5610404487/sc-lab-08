package Cal_Tax;

public interface Taxable {
	
	double getTax();
}