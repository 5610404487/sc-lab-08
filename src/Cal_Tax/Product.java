package Cal_Tax;

public class Product implements Taxable,Measurable{
	private String name_pro;
	private int value;
	
	public Product(String name_pro,int value){
		this.name_pro = name_pro;
		this.value = value;
	}
	@Override
	public double getMeasure() {
		return this.value;
	}
	public double getTax() {
		double sum = 0;
		return sum = this.value*0.07;
	}
}