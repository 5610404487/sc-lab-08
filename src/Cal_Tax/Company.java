package Cal_Tax;

public class Company implements Taxable,Measurable{
	private String name_com;
	private int income;
	private int expense;
	
	public Company(String name_com, int income, int expense){
		this.name_com = name_com;
		this.income = income;
		this.expense = expense;
	}
	@Override
	public double getMeasure() {
		return this.income;
	}
	public double getTax() {
		double sum = 0;
		sum = this.income-this.expense;
		return sum = sum*0.3;
	}
}