import java.util.ArrayList;

import Cal_Tax.Company;
import Cal_Tax.Person;
import Cal_Tax.Product;
import Cal_Tax.TaxCalculator;
import Cal_Tax.Taxable;

public class Test {
	
	public static void main(String[] args){
		Test test = new Test();
		System.out.println("1.");
		test.testPerson();
		System.out.println("");
		System.out.println("2.");
		test.testMin();
		System.out.println("");
		System.out.println("3.");
		test.testTax();
		
	}
	
	public void testPerson(){
		Measurable[] persons = new Measurable[3];
		persons[0]= new Persons("A",155);
		persons[1]= new Persons("B",165);
		persons[2]= new Persons("C",185);
		System.out.println("A: "+persons[0].getMeasure());
		System.out.println("B: "+persons[1].getMeasure());
		System.out.println("C: "+persons[2].getMeasure());
//		Data data = new Data();
		System.out.println("Average: "+Data.average(persons));
	}
	public void testMin(){
		Measurable person = new Persons("A",155);
		System.out.println("Person: "+person.getMeasure());
		Measurable countries = new Country("Thailand", 513000, 70, 387);
		System.out.println("Country: "+countries.getMeasure());
		Measurable accounts = new BankAccount(2000);
		System.out.println("BankAccount: "+accounts.getMeasure());
		Measurable mMin = Data.min(person, countries);
		Measurable mMin2 = Data.min(accounts, mMin);
		System.out.println("Min : "+Data.toString(mMin2));
	}
	public void testTax(){
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		persons.add(new Person("A",300000));
		persons.add(new Person("B",500000));
		persons.add(new Person("C",100000));
		ArrayList<Taxable> company = new ArrayList<Taxable>();
		company.add(new Company("D",1000000,800000));
		company.add(new Company("E",2000000,900000));
		company.add(new Company("F",3000000,1000000));
		ArrayList<Taxable> product = new ArrayList<Taxable>();
		product.add(new Product("G",299));
		product.add(new Product("H",1990));
		product.add(new Product("I",590));
//		ArrayList<Taxable> all = new ArrayList<Taxable>();
//		persons.add(new Person("A",300000));
//		company.add(new Company("E",2000000,900000));
//		product.add(new Product("I",590));
		System.out.println("Person: "+TaxCalculator.sum(persons));
		System.out.println("Company: "+TaxCalculator.sum(company));
		System.out.println("Product: "+TaxCalculator.sum(product));
//		System.out.println("3 type: "+TaxCalculator.sum(all));
		
	}
}