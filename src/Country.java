
public class Country implements Measurable {

	private String name;
	private double area;
	private int numPeople;
	private double gdp;
	
	
	public Country(String na, double a, int n, double g) {
		name = na;
		area = a;
		numPeople = n;
		gdp = g;
	}
	
	public String getName() {
		return name;
	}
	public double getArea() {
		return area;
	}
	public int getNumPeople() {
		return numPeople;
	}
	public double getGDP() {
		return gdp;
	}

	@Override
	public double getMeasure() {
		return area;
	}
	
	public String toString() {
		return "Country[name="+this.name+", area="+this.area+", people="+this.numPeople+", GDP="+this.gdp+"]";
	}

}
